import org.jetbrains.compose.desktop.application.dsl.TargetFormat

@Suppress("DSL_SCOPE_VIOLATION") // TODO: Remove once KTIJ-19369 is fixed
plugins {
    alias(libs.plugins.kotlin.multiplatform)
    alias(libs.plugins.compose)
    alias(libs.plugins.kotlin.kover)
    alias(libs.plugins.dokka)
}


group = "nl.entreco"
version = "1.0-SNAPSHOT"

kotlin {
    jvmToolchain(libs.versions.jvm.toolchain.get().toInt())
    jvm {
        withJava()
        testRuns["test"].executionTask.configure {
            useJUnitPlatform()
        }
    }

    sourceSets {

        val commonMain by getting {
            dependencies {
                implementation(project(":comm"))
                implementation(libs.ktor.server.core)
                implementation(libs.ktor.server.netty)
                implementation(libs.ktor.server.websockets)
                implementation(libs.kotlinx.datetime)
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
                implementation(libs.kotlinx.coroutines.test)
                implementation(libs.mockk)
                implementation(libs.turbine)
            }
        }
        val jvmMain by getting {
            dependencies {
                implementation(compose.desktop.currentOs)
                implementation(libs.rxusb)
                implementation(libs.serial.comm)


//                implementation(libs.ktor.server.websockets)
//                implementation(libs.ktor.server.core)
//                implementation(libs.ktor.server.netty)
//
//                implementation(libs.ktor.client.core)
//                implementation(libs.ktor.client.websockets)
            }
        }
        val jvmTest by getting {
            dependsOn(commonTest)
            dependencies {
                // Jvm Specific Test dependencies
            }
        }
    }
}

compose.desktop {
    application {
        mainClass = "MainKt"
        nativeDistributions {
            targetFormats(TargetFormat.Dmg, TargetFormat.Msi, TargetFormat.Deb)
            packageName = "BlueLib"
            packageVersion = "1.0.0"
        }
    }
}
