package com.salvia

import MainViewModel
import app.cash.turbine.test
import console.Log
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import server.BleServer
import server.WebServer
import kotlin.test.Ignore

class MainViewModelTest {

    private val mockWebServer: WebServer = mockk()
    private val mockBleServer: BleServer = mockk()

    @Test
    @Ignore("Needs some Flow Magig")
    fun `should send logs to the console`() = runTest {
        val subject = givenSubject()
        subject.logs().test {

            assertEquals(emptyList<String>(), awaitItem())

            subject.connect(this)
            assertEquals(listOf("Server running"), awaitItem().map { it.message })
            assertEquals(listOf("Server running", "one"), awaitItem().map { it.message })
            assertEquals(listOf("Server running", "one", "two"), awaitItem().map { it.message })
            assertEquals(listOf("Server running", "one", "two", "1"), awaitItem().map { it.message })
            assertEquals(listOf("Server running", "one", "two", "1", "2"), awaitItem().map { it.message })
            assertEquals(
                listOf("Server running", "one", "two", "1", "2", "Shutting down"),
                awaitItem().map { it.message })
            subject.reset()
            assertEquals(emptyList<String>(), awaitItem())
        }
    }

    @Test
    fun `should reset logs`() {
        val subject = givenSubject()
        subject.reset()
        assertTrue(subject.logs().value.isEmpty())
    }

    private fun givenSubject(): MainViewModel {
        coEvery { mockWebServer.start() } returns Unit
        coEvery { mockBleServer.start() } returns Unit
        coEvery { mockWebServer.logs() } returns flowOf(Log("1"), Log("2"))
        coEvery { mockBleServer.logs() } returns flowOf(Log("one"), Log("two"))
        coEvery { mockWebServer.stop() } returns Unit
        coEvery { mockBleServer.stop() } returns Unit
        return MainViewModel(mockWebServer, mockBleServer)
    }
}