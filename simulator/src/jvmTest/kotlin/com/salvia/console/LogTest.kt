package com.salvia.console

import console.Log
import kotlinx.datetime.Instant
import org.junit.jupiter.api.Test
import kotlin.random.Random
import kotlin.test.assertEquals

class LogTest {

    @Test
    fun `should get correct day component`() {
        val august31st2023 = Instant.fromEpochSeconds(1693471701)
        val subject = givenLog(august31st2023)
        val result = subject.day()
        assertEquals(31, result.dayOfMonth)
    }

    private fun givenLog(instant: Instant): Log = Log("This is a random log ${Random.nextFloat()}", timeStamp = instant)
}