package console

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow

class ConsoleLogger : ILoggable {
    private val messages: MutableSharedFlow<Log> = MutableSharedFlow()

    override suspend fun sendToConsole(log: Log) {
        messages.emit(log)
    }

    override fun logs(): Flow<Log> = messages
}