import console.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import server.BleServer
import server.WebServer

class MainViewModel(
    private val webServer: WebServer,
    private val bleServer: BleServer,
) {

    private val _logs = MutableStateFlow(listOf<Log>())
    fun logs(): StateFlow<List<Log>> = _logs

    fun reset() {
        _logs.value = emptyList()
    }

    fun connect(scope: CoroutineScope) {
        scope.launch { webServer.start() }
        scope.launch { bleServer.start() }

        scope.launch {
            merge(bleServer.logs(), webServer.logs())
                .onStart { _logs.value += Log("Server running") }
                .onEach { _logs.value += it }
                .onCompletion {
                    bleServer.stop()
                    webServer.stop()
                }
                .collect()
        }
    }
}
