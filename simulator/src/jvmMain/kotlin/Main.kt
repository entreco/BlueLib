@file:OptIn(ExperimentalFoundationApi::class)

import androidx.compose.animation.core.animateDp
import androidx.compose.animation.core.updateTransition
import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ClipboardManager
import androidx.compose.ui.platform.LocalClipboardManager
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Tray
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import androidx.compose.ui.window.rememberWindowState
import console.ConsoleLogger
import console.Log
import kotlinx.coroutines.launch
import server.BleServer
import server.WebServer

@Composable
@Preview
fun App(viewModel: MainViewModel) {
    val logs = viewModel.logs().collectAsState()
    val scrollState = rememberLazyListState()

    println("Logs: ${logs.value.joinToString()}")

    // Connect once on Init
    LaunchedEffect("Init") {
        viewModel.connect(this)
    }

    // Scroll to bottom on Changes
    LaunchedEffect(logs.value.size) {
        scrollState.animateScrollToItem(logs.value.size)
    }

    MaterialTheme {
        Column(Modifier.fillMaxSize()) {
            Button(
                modifier = Modifier.align(Alignment.CenterHorizontally),
                onClick = { viewModel.reset() }
            ) {
                Text("Reset")
            }
            LogViewer(
                logs = logs.value,
                modifier = Modifier.weight(1f),
                scrollState = scrollState,
            )
        }
    }
}

@Composable
fun LogViewer(
    logs: List<Log>,
    scrollState: LazyListState,
    modifier: Modifier = Modifier
) {
    val scope = rememberCoroutineScope()
    Box(modifier = modifier) {

        LazyColumn(
            state = scrollState,
            modifier = Modifier
                .testTag("LogTestTag")
                .fillMaxSize(1f)
                .padding(6.dp)
        ) {
            logs.groupBy { it.day() }.forEach { (date, logs) ->

                item {
                    DayHeader(date.toString())
                }

                items(logs) { log ->

                    LogEntry(
                        onClick = { },
                        log = log,
                    )
                }
            }
        }

        // Show the button if we can scroll forward (is down)
        // greater than the threshold.
        val jumpToBottomButtonEnabled by remember {
            derivedStateOf { scrollState.canScrollForward }
        }

        JumpToBottom(
            // Only show if the scroller is not at the bottom
            enabled = jumpToBottomButtonEnabled,
            onClicked = {
                scope.launch {
                    scrollState.animateScrollToItem(logs.size)
                }
            },
            modifier = Modifier.align(Alignment.BottomCenter)
        )
    }
}


@Composable
fun LogEntry(
    onClick: (String) -> Unit,
    log: Log,
) {
    val spacing = Modifier
    Row(modifier = spacing) {
        Entry(
            log = log,
            modifier = Modifier
                .fillMaxSize()
                .padding(end = 8.dp)
                .weight(1f)
        )
    }
}

@Composable
fun Entry(
    log: Log,
    modifier: Modifier = Modifier
) {
    var selected by remember { mutableStateOf(false) }
    val clipboard: ClipboardManager = LocalClipboardManager.current
    val background = if (selected) modifier.background(Color.LightGray) else modifier

    Row(modifier = background.clickable {
        selected = !selected
        clipboard.setText(AnnotatedString(log.toString()))
    }) {
        Text(
            text = log.time().toString().take(15),
            color = Color.LightGray,
            fontSize = MaterialTheme.typography.subtitle2.fontSize
        )
        Spacer(Modifier.padding(horizontal = 2.dp))
        Text(text = log.symbol(), fontSize = MaterialTheme.typography.subtitle2.fontSize)
        Spacer(Modifier.padding(horizontal = 2.dp))
        Text(text = log.message, fontSize = MaterialTheme.typography.subtitle2.fontSize)
    }
}

@Composable
fun DayHeader(dayString: String) {
    Row(
        modifier = Modifier
            .padding(vertical = 8.dp, horizontal = 16.dp)
            .height(16.dp)
    ) {
        DayHeaderLine()
        Text(
            text = dayString,
            modifier = Modifier.padding(horizontal = 16.dp),
            style = MaterialTheme.typography.subtitle2,
            color = MaterialTheme.colors.onSurface
        )
        DayHeaderLine()
    }
}

@Composable
private fun RowScope.DayHeaderLine() {
    Divider(
        modifier = Modifier
            .weight(1f)
            .align(Alignment.CenterVertically),
        color = MaterialTheme.colors.onSurface.copy(alpha = 0.12f)
    )
}

private enum class Visibility {
    VISIBLE,
    GONE
}

/**
 * Shows a button that lets the user scroll to the bottom.
 */
@Composable
fun JumpToBottom(
    enabled: Boolean,
    onClicked: () -> Unit,
    modifier: Modifier = Modifier
) {
    // Show Jump to Bottom button
    val transition = updateTransition(
        if (enabled) Visibility.VISIBLE else Visibility.GONE,
        label = "JumpToBottom visibility animation"
    )
    val bottomOffset by transition.animateDp(label = "JumpToBottom offset animation") {
        if (it == Visibility.GONE) {
            (-32).dp
        } else {
            32.dp
        }
    }
    if (bottomOffset > 0.dp) {
        ExtendedFloatingActionButton(
            icon = {
                Icon(
                    imageVector = Icons.Filled.ArrowDropDown,
                    modifier = Modifier.height(18.dp),
                    contentDescription = null
                )
            },
            text = {
                Text(text = "Scroll to Bottom")
            },
            onClick = onClicked,
            contentColor = MaterialTheme.colors.onPrimary,
            modifier = modifier
                .offset(x = 0.dp, y = -bottomOffset)
                .height(36.dp)
        )
    }
}

fun main() = application {
    val viewModel = MainViewModel(WebServer(ConsoleLogger()), BleServer(ConsoleLogger()))
    val icon = painterResource("logo.svg")
    Tray(
        icon = icon,
        menu = {
            Item("Quit App", onClick = ::exitApplication)
        }
    )
    Window(
        onCloseRequest = ::exitApplication,
        title = "Simulator",
        icon = icon,
        state = rememberWindowState(width = 800.dp, height = 600.dp)
    ) {
        App(viewModel)
    }
}
