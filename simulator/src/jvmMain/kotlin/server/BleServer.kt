package server

import com.fazecast.jSerialComm.SerialPort
import com.salvia.blue.Request
import com.salvia.blue.toResponse
import console.ILoggable
import console.Icon
import console.Log
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlin.coroutines.coroutineContext

class BleServer(
    private val console: ILoggable
) : IServer, ILoggable by console {

    object Settings {
        const val DONGLE_DESCRIPTOR = "nRF52"
        const val BAUD_RATE = 115200
        const val POLL_DELAY_MS = 200L
    }

    private var port: SerialPort? =
        SerialPort.getCommPorts().firstOrNull { it.portDescription.contains(Settings.DONGLE_DESCRIPTOR) }?.apply {
            // Ensure correct settings are used
            setDTR()
            setRTS()
            baudRate = Settings.BAUD_RATE
        }

    override suspend fun start() {
        port?.apply {

            // Open Port
            openPort()

            // Disable Timeouts -> we use NON-Blocking MODE
            // https://github.com/Fazecast/jSerialComm/wiki/Nonblocking-Reading-Usage-Example
            setComPortTimeouts(SerialPort.TIMEOUT_NONBLOCKING, 0, 0)

            while (isOpen && coroutineContext.isActive) {

                while (bytesAvailable() == 0) {
                    delay(Settings.POLL_DELAY_MS)
                }

                val buffer = ByteArray(bytesAvailable())
                readBytes(buffer, buffer.size)
                val string = String(buffer)
                val requestId = string.takeWhile { it.isDigit() }.toUInt()
                val requestPayload = string.dropWhile { it.isDigit() }.toByteArray()
                val request = Request(requestId, 10, requestPayload)
                sendToConsole(Log(request.toString(), Icon("⇨")))

                val response = request.toResponse()
                val numWritten = writeBytes(response.bytes, response.bytes.size)
                println("BLE: $response bytes written: $numWritten")
                sendToConsole(Log(response.toString(), Icon("⇦")))
            }

            stop()
        }
    }

    override suspend fun stop() {
        port?.closePort()
    }
}