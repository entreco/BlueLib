package server

import com.salvia.blue.Request
import com.salvia.blue.toResponse
import console.ILoggable
import console.Icon
import console.Log
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

class WebServer(
    private val console: ILoggable
) : IServer, ILoggable by console {

    object Settings {
        const val PORT = 8080
    }

    private var server: NettyApplicationEngine = embeddedServer(Netty, port = Settings.PORT) {
        routing {
            get("/comm/{id}") {
                val requestId = call.parameters["id"]?.toUInt() ?: 0U
                val requestPayload = call.receiveText().toByteArray()

                val request = Request(requestId, 10, requestPayload)
                sendToConsole(Log(request.toString(), Icon("⇨")))

                val response = request.toResponse()
                call.respondBytes { response.bytes }
                sendToConsole(Log(response.toString(), Icon("⇦")))
            }
        }
    }

    override suspend fun start() {
        server.start()
    }

    override suspend fun stop() {
        server.stop()
    }
}