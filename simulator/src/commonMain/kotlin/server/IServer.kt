package server

interface IServer {
    suspend fun start()
    suspend fun stop()
}