package console

import androidx.compose.runtime.Immutable
import kotlinx.datetime.*


@Immutable
data class Log(
    val message: String,
    private val icon: Icon = Icon("⊹"),
    private val timeStamp: Instant = Clock.System.now()
) {
    fun symbol(): String = icon()

    fun day(zone: TimeZone = TimeZone.currentSystemDefault()): LocalDate = timeStamp.toLocalDateTime(zone).date

    fun time(zone: TimeZone = TimeZone.currentSystemDefault()): LocalTime = timeStamp.toLocalDateTime(zone).time

    override fun toString(): String {
        return "$timeStamp ${symbol()} $message"
    }
}

@JvmInline
value class Icon(private val unicode: String) {
    operator fun invoke(): String = unicode.take(1)
}