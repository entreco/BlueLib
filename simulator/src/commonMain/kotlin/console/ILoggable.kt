package console

import kotlinx.coroutines.flow.Flow

interface ILoggable {
    suspend fun sendToConsole(log: Log)
    fun logs(): Flow<Log>
}