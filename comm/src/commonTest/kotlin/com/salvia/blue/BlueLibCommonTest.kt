package com.salvia.blue

import kotlin.test.Test
import kotlin.test.assertNotNull

class BlueLibCommonTest {

    private val blueLib = IChannel.Builder().build()

    @Test
    fun isNotNull() {
        assertNotNull(blueLib)
    }

}