package com.salvia.blue

import io.ktor.client.request.*
import io.ktor.utils.io.core.*

public data class Request(
    public val id: UInt,
    public val timeOut: Long,
    public val bytes: ByteArray,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as Request

        if (id != other.id) return false
        if (timeOut != other.timeOut) return false
        return bytes.contentEquals(other.bytes)
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + timeOut.hashCode()
        result = 31 * result + bytes.contentHashCode()
        return result
    }
}
