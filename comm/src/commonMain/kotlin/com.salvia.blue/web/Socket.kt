package com.salvia.blue.web

import com.salvia.blue.IChannel
import com.salvia.blue.Request
import com.salvia.blue.Response

import kotlinx.coroutines.withTimeout

internal class Socket(
    private val builder: IChannel.Builder
) : IChannel {

//    private var session: DefaultClientWebSocketSession? = null
//    private val client = HttpClient {
//        install(WebSockets)
//    }

    override suspend fun connect() {
//        session = client.webSocketSession(method = HttpMethod.Get, host = builder.host, port = builder.port, path = "/comm")
    }

    override suspend fun send(request: Request): Response {
//        session?.send(request.bytes)
        return withTimeout(request.timeOut) {
//            val response = session?.incoming?.receive() as Frame.Binary?
            Response(request.id, byteArrayOf())
        }
    }

    override suspend fun close() {
//        session?.close()
//        session = null
//        client.close()

    }
}