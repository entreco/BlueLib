package com.salvia.blue

import com.salvia.blue.ble.BleGatt
import com.salvia.blue.web.Socket

public interface IChannel {
    public suspend fun connect()
    public suspend fun send(request: Request): Response
    public suspend fun close()

    private class BleChannel(private val builder: Builder) : IChannel by BleGatt(builder)
    private class HttpChannel(private val builder: Builder) : IChannel by Socket(builder)

    public class Builder {

        internal var port: Int? = null
        internal var host: String? = null
        public fun withWeb(port: Int, host: String): Builder = apply {
            this.port = port
            this.host = host
        }

        public fun build(): IChannel = when {
            port != null && host != null -> HttpChannel(this)
            else -> BleChannel(this)
        }
    }
}

