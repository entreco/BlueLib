package com.salvia.blue

public data class Response(
    public val requestId: UInt,
    public val bytes: ByteArray
){
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as Response

        if (requestId != other.requestId) return false
        return bytes.contentEquals(other.bytes)
    }

    override fun hashCode(): Int {
        var result = requestId.hashCode()
        result = 31 * result + bytes.contentHashCode()
        return result
    }

}