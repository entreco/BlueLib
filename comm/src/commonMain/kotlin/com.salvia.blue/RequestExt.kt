package com.salvia.blue

import io.ktor.utils.io.core.*

public fun Request.toResponse(): Response = when (id) {
    1U -> Response(id, "Pong".toByteArray())
    else -> Response(0U, "Unknown request:${id}".toByteArray())
}