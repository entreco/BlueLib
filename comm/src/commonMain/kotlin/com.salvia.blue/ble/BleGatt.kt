package com.salvia.blue.ble

import com.salvia.blue.IChannel
import com.salvia.blue.Request
import com.salvia.blue.Response
import kotlinx.coroutines.delay
import kotlin.random.Random

internal class BleGatt(
    private val builder: IChannel.Builder
) : IChannel {
    override suspend fun connect() {
        TODO("Not yet implemented")
    }

    override suspend fun send(request: Request): Response {
        delay(Random.nextLong(500, 5000))
        return Response(request.id, byteArrayOf())
    }

    override suspend fun close() {
        TODO("Not yet implemented")
    }
}