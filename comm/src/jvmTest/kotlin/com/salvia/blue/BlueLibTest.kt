package com.salvia.blue

import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class BlueLibTest {

    @Nested
    @DisplayName("Given default instance")
    inner class Given {

        private val blueLib = IChannel.Builder().build()

        @Test
        @DisplayName("can be created")
        fun isNotNull() {
            assertNotNull(blueLib)
        }

        @Nested
        @DisplayName("When sending a Simple Request")
        inner class When {

            private val request = Request(1U, 10L, byteArrayOf())

            @Test
            @DisplayName("Then we get a SimpleResponse")
            fun `when sending a Simple Request`() = runTest {
                val response = blueLib.send(request)
                assertNotNull(response)
            }
        }

        @Nested
        @DisplayName("When sending lots of Simple Request")
        inner class Lots {

            @Test
            @DisplayName("Then all Responses are still in the original order")
            fun `when sending a Simple Request`() = runTest {
                val responses = (1U until 1000U).map {
                    blueLib.send(Request(it, 0L, byteArrayOf()))
                }

                val sorted = responses.sortedBy { it.requestId }
                assertEquals(responses, sorted)
            }
        }
    }
}